<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    protected $table = 'posts';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function(Blueprint $table){
            $table->increments('id');
            $table->string('title', 150);
            $table->string('slug', 150)->unique();
            $table->string('link', 250)->nullable();
            $table->text('excerpt')->nullable();
            $table->text('content')->nullable();
            $table->text('image')->nullable();
            $table->text('gallery')->nullable();
            $table->string('post_type', 100);
            $table->integer('author');
            $table->boolean('feature')->default(0)->comment('Yes: 1, other: 0');
            $table->integer('order')->default(0);
            $table->integer('status')->comment('public: 2, draft: 1, trash: 0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
