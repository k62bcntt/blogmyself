<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCommentsTable.
 */
class CreateLikesharePostsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('likeshare_posts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned()->index();;
            $table->foreign('post_id')->references('id')->on('posts')->onDelte('cascade');
            $table->integer('like')->unsigned();
            $table->integer('share')->unsigned();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('likeshare_posts');
	}
}
