<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    protected $table = 'categories';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function(Blueprint $table){
            $table->increments('id');
            $table->string('name',100);
            $table->string('slug', 100);
            $table->integer('parent_id')->default(0);
            $table->text('desciption')->nullable();
            $table->text('icon', 200)->nullable();
            $table->integer('order')->default(0);
            $table->string('status')->default(1)->comment('publish: 1, private: 0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
