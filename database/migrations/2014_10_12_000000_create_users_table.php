<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('display_name', 100);
            $table->text('avatar')->nullable();
            $table->string('email', 100)->unique();
            $table->string('password', 200);
            $table->boolean('gender')->default(0)->comment('0:male , 1:female');
            $table->string('phone', 50)->nullable();
            $table->date('dob');
            $table->text('address');
            $table->string('email_verified', 100)->default(0);
            $table->integer('status')->comment('active: 1, deactive: 0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
