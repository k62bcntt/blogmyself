<?php

use App\Entities\Post;
use App\Entities\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(Post::class, function (Faker $faker) {
    $images = [
        '/assets/images/demo/products/demo1.jpg',
        '/assets/images/demo/products/demo2.jpg',
        '/assets/images/demo/products/demo3.jpg',
        '/assets/images/demo/products/demo4.jpg',
        '/assets/images/demo/products/demo5.jpg',
    ];

    $user = User::where('email', '=', 'admin@gmail.com')->first();

    return [
        'id'      => $faker->unique()->numberBetween($min = 1, $max = 999),
        'title'   => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'slug'    => $faker->slug,
        'image'   => $faker->randomElement($images),
        'content' => $faker->paragraphs($nb = 30, $asText = true),
        'status'  => Post::STATUS_ACTIVE,
        'user_id' => $user->id,
    ];
});
