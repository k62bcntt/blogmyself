<?php

use App\Entities\Category;
use App\Entities\Post;
use App\Entities\Tag;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $categories = Category::all(['id']);

        // factory(Post::class, 30)->create()->each(function ($product) use ($categories) {
        //     $product->categories()->sync([$categories->random()->id]);
        // });
        $categories = Category::where('status', 1)->get();
        $tags       = Tag::where('status', 1)->get();

        $faker = new \Faker\Generator();
        $faker->addProvider(new \Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new \Faker\Provider\en_US\Address($faker));
        $faker->addProvider(new \Faker\Provider\en_US\PhoneNumber($faker));
        $faker->addProvider(new \Faker\Provider\Internet($faker));
        $faker->addProvider(new \Faker\Provider\DateTime($faker));
        $faker->addProvider(new \Faker\Provider\Lorem($faker));

        for ($i = 0; $i < 30; $i++) {
            $post            = new Post();
            $post->title     = $faker->sentence(rand(6,10), true);
            $post->slug      = str_slug($post->title);
            $post->link      = '';
            $post->excerpt   = $faker->sentence(20, true);
            $post->content   = $faker->text;
            $post->post_type = 'post';
            $post->author    = 1;
            $post->feature   = 0;
            $post->order     = 0;
            $post->status    = Post::PUBLISH;
            $post->save();
            $post->categories()->sync([$categories->random()->id]);
            $post->tags()->sync([$tags->random()->id, $tags->random()->id, $tags->random()->id]);
        }
    }
}
