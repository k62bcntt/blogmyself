<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name'        => 'create.admin',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'update.admin',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'view.admin',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'delete.admin',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'create.role',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'update.role',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'view.role',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'delete.role',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'create.user',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'update.user',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'view.user',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'delete.user',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'create.post',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'update.post',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'view.post',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'delete.post',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'create.category',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'update.category',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'view.category',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'delete.category',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'create.tag',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'update.tag',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'view.tag',
                'description' => '',
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'delete.tag',
                'description' => '',
                'guard_name'  => 'api',
            ],
        ];
        foreach ($permissions as $key => $permission) {
            $createPer = Permission::create([
                'name'       => $permission['name'],
                'guard_name' => $permission['guard_name'],
            ]);
        }
    }
}
