<?php

use App\Entities\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$categories = [
    		[
    			'name' => 'Angular2',
    			'parent_id' => 0
    		],
    		[
    			'name' => 'ReactJs',
    			'parent_id' => 0
    		],
    		[
    			'name' => 'React Native',
    			'parent_id' => 0
    		],
    		[
    			'name' => 'Wordpress',
    			'parent_id' => 0
    		],
    		[
    			'name' => 'Server',
    			'parent_id' => 0
    		],
    		[
    			'name' => 'Html - Css - Jquery',
    			'parent_id' => 0
    		],
    		[
    			'name' => 'Server',
    			'parent_id' => 0
    		],
    	];

		$faker = new \Faker\Generator();
		$faker->addProvider(new \Faker\Provider\Lorem($faker));	
    	foreach ($categories as $key => $cate) {
    		Category::create([
	        	'name' => $cate['name'],
	        	'slug' => str_slug($cate['name']),
	        	'parent_id' => $cate['parent_id'],
	        	'desciption' => $faker->text
	        ]);
    	}
    }
}
