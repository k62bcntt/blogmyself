<?php

use App\Entities\User;
use Illuminate\Database\Eloquent\Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin = Role::where('name', 'superadmin')->first();
        $admin      = Role::where('name', 'admin')->first();
        $manager    = Role::where('name', 'manager')->first();
        $moderator  = Role::where('name', 'moderator')->first();
        $user_role  = Role::where('name', 'user')->first();
        $arr_admin  = [
            [
                'name'  => 'Super Admin',
                'email' => 'superadmin@gmail.com',
                'role'  => 'superadmin',
            ],
            [
                'name'  => 'Admin',
                'email' => 'admin@gmail.com',
                'role'  => 'admin',
            ],
            [
                'name'  => 'Manager',
                'email' => 'manager@gmail.com',
                'role'  => 'manager',
            ],
            [
                'name'  => 'Moderator',
                'email' => 'moderator@gmail.com',
                'role'  => 'moderator',
            ],
        ];
        foreach ($arr_admin as $key => $val) {
            $faker = new \Faker\Generator();
            $faker->addProvider(new \Faker\Provider\en_US\Person($faker));
            $faker->addProvider(new \Faker\Provider\en_US\Address($faker));
            $faker->addProvider(new \Faker\Provider\en_US\PhoneNumber($faker));
            $faker->addProvider(new \Faker\Provider\Internet($faker));
            $faker->addProvider(new \Faker\Provider\DateTime($faker));

            $user               = new User();
            $user->name         = $val['name'];
            $user->display_name = $val['name'];
            $user->email        = $val['email'];
            $user->password     = Hash::make('secret');
            $user->gender       = mt_rand(0, 1);
            $user->dob          = $faker->date('Y-m-d', '-15 years');
            $user->phone        = $faker->phoneNumber;
            $user->address      = $faker->address;
            $user->status       = User::ACTIVE;
            $user->save();
            $user->assignRole(${$val['role']}->pluck('name'));
        }

        for ($i = 0; $i < 20; $i++) {
            $faker = new \Faker\Generator();
            $faker->addProvider(new \Faker\Provider\en_US\Person($faker));
            $faker->addProvider(new \Faker\Provider\en_US\Address($faker));
            $faker->addProvider(new \Faker\Provider\en_US\PhoneNumber($faker));
            $faker->addProvider(new \Faker\Provider\en_US\Company($faker));
            $faker->addProvider(new \Faker\Provider\Lorem($faker));
            $faker->addProvider(new \Faker\Provider\Internet($faker));
            $faker->addProvider(new \Faker\Provider\DateTime($faker));

            $user         = new User();
            $user->gender = mt_rand(0, 1);
            if ($user->gender == 0) {
                $gender = 'male';
            } else {
                $gender = 'female';
            }
            $user->name         = $faker->firstName($gender) . ' ' . $faker->lastName($gender);
            $user->display_name = $user->name;
            $user->email        = $faker->email;
            $user->password     = bcrypt('secret');
            $user->dob          = $faker->date('Y-m-d', '-15 years');
            $user->phone        = $faker->phoneNumber;
            $user->address      = $faker->address;
            $user->status       = User::ACTIVE;
            $user->save();
            $user->assignRole($user_role->pluck('name'));
        }
    }
}
