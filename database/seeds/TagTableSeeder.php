<?php

use App\Entities\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
    		[
    			'name' => 'HTML'
    		],
    		[
    			'name' => 'CSS'
    		],
    		[
    			'name' => 'React Native'
    		],
    		[
    			'name' => 'Wordpress'
    		],
    		[
    			'name' => 'Server'
    		],
    		[
    			'name' => 'JQuery'
    		],
    		[
    			'name' => 'Plugin'
    		],
    		[
    			'name' => 'Lập trình'
    		],
    		[
    			'name' => 'Ngôn ngữ'
    		],
    		[
    			'name' => 'SEO'
    		],
    		[
    			'name' => 'Website'
    		]
    	];

		$faker = new \Faker\Generator();
		$faker->addProvider(new \Faker\Provider\Lorem($faker));	
    	foreach ($tags as $key => $tag) {
    		Tag::create([
	        	'name' => $tag['name'],
	        	'slug' => str_slug($tag['name'])
	        ]);
    	}
    }
}
