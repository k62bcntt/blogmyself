<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_superadmin = Permission::all();
        $permission_admin      = $permission_superadmin->filter(function ($item) {
            return ($item['name'] != 'create.admin' && $item['name'] != 'update.admin' && $item['name'] != 'delete.admin');
        });
        $permission_manager = $permission_admin->filter(function ($item) {
            return ($item['name'] != 'create.role' && $item['name'] != 'update.role' && $item['name'] != 'delete.role');
        });
        $permission_moderator = $permission_manager->filter(function ($item) {
            return ($item['name'] != 'create.user' && $item['name'] != 'update.user' && $item['name'] != 'delete.user');
        });
        $permission_user = $permission_moderator->filter(function ($item) {
            return ($item['name'] != 'delete.post' && $item['name'] != 'delete.category' && $item['name'] != 'delete.tag');
        });
        $roles = [
            [
                'name'        => 'superadmin',
                'description' => '',
                'level'       => 1,
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'admin',
                'description' => '',
                'level'       => 2,
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'manager',
                'description' => '',
                'level'       => 3,
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'moderator',
                'description' => '',
                'level'       => 4,
                'guard_name'  => 'api',
            ],
            [
                'name'        => 'user',
                'description' => '',
                'level'       => 5,
                'guard_name'  => 'api',
            ],
        ];
        foreach ($roles as $key => $role) {
            $create_role = Role::create(
                [
                    'name'       => $role['name'],
                    'guard_name' => $role['guard_name'],
                ]
            );
            switch ($role['level']) {
                case 1:
                    $create_role->givePermissionTo($permission_superadmin->pluck('name')->all());
                    break;
                case 2:
                    $create_role->givePermissionTo($permission_admin->pluck('name')->all());
                    break;
                case 3:
                    $create_role->givePermissionTo($permission_manager->pluck('name')->all());
                    break;
                case 4:
                    $create_role->givePermissionTo($permission_moderator->pluck('name')->all());
                    break;
                case 5:
                    $create_role->givePermissionTo($permission_user->pluck('name')->all());
                    break;
                default:
                    $create_role->givePermissionTo($permission_user->pluck('name')->all());
                    break;
            }
        }
    }
}
