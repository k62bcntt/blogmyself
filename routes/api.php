<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->group(['prefix' => 'admin'], function ($api) {
        // Auth
        $api->group(['prefix' => 'auth'], function ($api) {
            $api->post('login', 'App\Http\Controllers\Api\Admin\AuthenticateController@authenticate');
        });

        // Upload file
        $api->post('upload/{id}', 'App\Http\Controllers\Api\FileController@upload');

        // Post
        $api->group(['prefix' => 'posts'], function ($api) {
            $api->get('list', 'App\Http\Controllers\Api\Admin\PostsController@index');
            $api->get('{id}', 'App\Http\Controllers\Api\Admin\PostsController@edit');
            $api->put('{id}', 'App\Http\Controllers\Api\Admin\PostsController@update');
            $api->post('create', 'App\Http\Controllers\Api\Admin\PostsController@store');
            $api->delete('delete/{id}', 'App\Http\Controllers\Api\Admin\PostsController@destroy');
        });

        // User
        $api->group(['prefix' => 'users'], function ($api) {
            $api->get('list', 'App\Http\Controllers\Api\Admin\UsersController@index');
            $api->get('{id}', 'App\Http\Controllers\Api\Admin\UsersController@edit');
            $api->put('{id}', 'App\Http\Controllers\Api\Admin\UsersController@update');
            $api->post('create', 'App\Http\Controllers\Api\Admin\UsersController@store');
            $api->delete('delete/{id}', 'App\Http\Controllers\Api\Admin\UsersController@destroy');
            $api->post('update-status/{id}', 'App\Http\Controllers\Api\Admin\UsersController@changeStatus');
            $api->post('update-password/{id}', 'App\Http\Controllers\Api\Admin\UsersController@changePassword');
        });

        //Category
        $api->group(['prefix' => 'categories'], function ($api) {
            $api->get('list', 'App\Http\Controllers\Api\Admin\CategoriesController@index');
            $api->get('{id}', 'App\Http\Controllers\Api\Admin\CategoriesController@edit');
            $api->put('{id}', 'App\Http\Controllers\Api\Admin\CategoriesController@update');
            $api->post('create', 'App\Http\Controllers\Api\Admin\CategoriesController@store');
            $api->delete('delete/{id}', 'App\Http\Controllers\Api\Admin\CategoriesController@destroy');
        });

        //Tag
        $api->group(['prefix' => 'tags'], function ($api) {
            $api->get('list', 'App\Http\Controllers\Api\Admin\TagsController@index');
            $api->get('{id}', 'App\Http\Controllers\Api\Admin\TagsController@edit');
            $api->put('{id}', 'App\Http\Controllers\Api\Admin\TagsController@update');
            $api->post('create', 'App\Http\Controllers\Api\Admin\TagsController@store');
            $api->delete('delete/{id}', 'App\Http\Controllers\Api\Admin\TagsController@destroy');
        });
    });
});
