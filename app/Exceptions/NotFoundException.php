<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class NotFoundException extends BadRequestHttpException
{
    public function __construct($entity = null)
    {
        if ($entity) {
            $message = $entity . ' not found';
        } else {
            $message = 'Not Found';
        }
        parent::__construct($message, null, 1002);
    }
}