<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    use Helpers;
    protected $upload_directory = 'uploads';

    public function success()
    {
        return $this->response->array(['success' => true]);
    }
}
