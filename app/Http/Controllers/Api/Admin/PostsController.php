<?php

namespace App\Http\Controllers\Api\Admin;

use App\Entities\Post;
use App\Http\Controllers\ApiController;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Repositories\PostRepository;
use App\Transformers\PostTransformer;
use App\Validators\PostValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class PostsController extends ApiController
{

    /**
     * @var PostRepository
     */
    protected $repository;

    /**
     * @var PostValidator
     */
    protected $validator;

    public function __construct(PostRepository $repository, PostValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Post;

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('title', 'like', "%{$search}%");
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $posts    = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new PostTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new PostTransformer;
        }

        return $this->response->paginator($posts, new PostTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PostCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $data             = $request->except(['q']);
            $count_exist_post = Post::where('slug', $data['slug'])->count();
            if ($count_exist_post > 0) {
                $data['slug'] = $data['slug'] . '-' . ($count_exist_post + 1);
            }
            $post = $this->repository->create($data);
            return $this->response->item($post, new PostTransformer);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $post = $this->repository->find($id);

        if ($request->has('includes')) {
            $transformer = new PostTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new PostTransformer;
        }
        return $this->response->item($post, $transformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $post = $this->repository->find($id);
        $transformer = new PostTransformer;
        return $this->response->item($post, $transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PostUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(PostUpdateRequest $request, $id)
    {
        $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
        $post = $this->repository->update($request->all(), $id);
        return $this->response->item($post, new PostTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if (!Auth::user()->can('delete.posts')) {
        //     throw new PermissionDeniedException;
        // }
        $this->repository->delete($id);
        return $this->success();
    }
}
