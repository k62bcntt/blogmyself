<?php

namespace App\Http\Controllers\Api\Admin;

use App\Exceptions\NotFoundException;
use App\Http\Controllers\ApiController;
use App\Repositories\UserRepository;
use App\Validators\AuthValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Prettus\Validator\Exceptions\ValidatorException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthenticateController extends ApiController
{
    public $repository;
    public $validator;

    public function __construct(UserRepository $repository, AuthValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
        $this->validator->with($credentials)->passesOrFail('LOGIN');

        try {
            $user = $this->repository->findByField('email', $credentials['email'])->first();
            if (!$user) {
                throw new NotFoundException("Email");
            }
            if (!Hash::check($credentials['password'], $user->password)) {
                throw new \Exception("Password does not match", 1001);
            }
            if (!$user->hasRole('admin') || !$user->hasRole('superadmin')) {
                throw new PermissionDeniedException();
            }
            if (!$token = JWTAuth::attempt($credentials)) {
                return Response::json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return Response::json(['error' => 'could_not_create_token'], 500);
        }

        return $this->response->array(compact('token'));
    }
}
