<?php

namespace App\Http\Controllers\Api\Admin;

use App\Entities\Tag;
use App\Http\Controllers\ApiController;
use App\Http\Requests\TagCreateRequest;
use App\Http\Requests\TagUpdateRequest;
use App\Repositories\TagRepository;
use App\Transformers\TagTransformer;
use App\Validators\TagValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class TagsController extends ApiController
{

    /**
     * @var TagRepository
     */
    protected $repository;

    /**
     * @var TagValidator
     */
    protected $validator;

    public function __construct(TagRepository $repository, TagValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Tag;

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('title', 'like', "%{$search}%");
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $tags     = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new TagTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new TagTransformer;
        }

        return $this->response->paginator($tags, new TagTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TagCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(TagCreateRequest $request)
    {

        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $data             = $request->except(['q']);
            $count_exist_post = Tag::where('slug', $data['slug'])->count();
            if ($count_exist_post > 0) {
                $data['slug'] = $data['slug'] . '-' . ($count_exist_post + 1);
            }
            $tag = $this->repository->create($data);
            return $this->response->item($tag, new TagTransformer);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $tag = $this->repository->find($id);
        if ($request->has('includes')) {
            $transformer = new TagTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new TagTransformer;
        }
        return $this->response->item($tag, $transformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $tag = $this->repository->find($id);
        if ($request->has('includes')) {
            $transformer = new TagTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new TagTransformer;
        }
        return $this->response->item($tag, $transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TagUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(TagUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $tag = $this->repository->update($request->all(), $id);
            return $this->response->item($tag, new TagTransformer);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);
        return $this->success();
    }
}
