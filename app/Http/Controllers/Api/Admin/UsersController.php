<?php

namespace App\Http\Controllers\Api\Admin;

use App\Entities\User;
use App\Http\Controllers\ApiController;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Prettus\Validator\Contracts\ValidatorInterface;
use Spatie\Permission\Models\Role;

class UsersController extends ApiController
{

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var UserValidator
     */
    protected $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        // $users = $this->repository->all();

        $query = new User;

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('title', 'like', "%{$search}%");
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $users    = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }

        return $this->response->paginator($users, new UserTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $this->validator->with($request->except(['password_confirmation', 'q']))->passesOrFail(ValidatorInterface::RULE_CREATE);
        $data             = $request->except(['password_confirmation', 'q']);
        $data['password'] = Hash::make($data['password']);
        $user = $this->repository->create($data);
        $role  = Role::where('name', 'user')->first();
        $user = $user->assignRole($role->pluck('name'));
        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->repository->find($id);

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $user,
            ]);
        }

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = $this->repository->find($id);

        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $this->validator->with($request->except(['id']))->passesOrFail(ValidatorInterface::RULE_UPDATE);
        $user = $this->repository->update($request->all(), $id);
        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);
        return $this->success();
    }

    /**
     * [changeStatus description]
     * @param  Request $request [description]
     * @param  integer  $id      [description]
     * @return boolean           [description]
     */
    public function changeStatus(Request $request, $id) {
        $status = $request->only('status');
        $user = $this->repository->update($status, $id);
        return $this->success();
    }

    /**
     * [changePassword description]
     * @param  Request $request [description]
     * @param  integer  $id      [description]
     * @return boolean           [description]
     */
    public function changePassword(Request $request, $id) {
        $password = $request->only('password');
        $user = $this->repository->update($password, $id);
        return $this->success();
    }
}
