<?php

namespace App\Http\Controllers\Api\Admin;

use App\Entities\Category;
use App\Http\Controllers\ApiController;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Repositories\CategoryRepository;
use App\Transformers\CategoryTransformer;
use App\Validators\CategoryValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class CategoriesController extends ApiController
{

    /**
     * @var CategoryRepository
     */
    protected $repository;

    /**
     * @var CategoryValidator
     */
    protected $validator;

    public function __construct(CategoryRepository $repository, CategoryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Category;

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('title', 'like', "%{$search}%");
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $posts    = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new CategoryTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new CategoryTransformer;
        }

        return $this->response->paginator($posts, new CategoryTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryCreateRequest $request)
    {

        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $data             = $request->except(['q']);
            $count_exist_post = Category::where('slug', $data['slug'])->count();
            if ($count_exist_post > 0) {
                $data['slug'] = $data['slug'] . '-' . ($count_exist_post + 1);
            }
            $category = $this->repository->create($data);
            return $this->response->item($category, new CategoryTransformer);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $category = $this->repository->find($id);
        if ($request->has('includes')) {
            $transformer = new CategoryTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new CategoryTransformer;
        }
        return $this->response->item($category, $transformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $category = $this->repository->find($id);
        if ($request->has('includes')) {
            $transformer = new CategoryTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new CategoryTransformer;
        }
        return $this->response->item($category, $transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoryUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
        $category = $this->repository->update($request->all(), $id);
        return $this->response->item($category, new CategoryTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);
        return $this->success();
    }
}
