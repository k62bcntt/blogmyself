<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use DateTime;
use Exception;
use Illuminate\Support\Facades\Storage;

class ExportController extends ApiController
{
    public function exportCSV($exports, $filename)
    {
        foreach ($exports as $key => $export) {
            $exports[$key] = (array) $export;
        }
        $time    = new DateTime;
        $year    = $time->format('Y');
        $month   = $time->format('m');
        $path    = "exports/{$year}/{$month}/" . md5(time()) . "_{$filename}";
        $success = Storage::disk('local')->put($path, '');

        $file = fopen($path, "w");

        if (empty($exports)) {
            throw new Exception("Data empty", 1);
        }

        fputcsv($file, array_keys($exports[0]), $delimiter = ',', $enclosure = '"');
        foreach ($exports as $export) {
            fputcsv($file, array_map(function ($value) {
                return trim($value);
            }, $export), $delimiter = ',', $enclosure = '"');
        }
        fclose($file);

        $url = url($path);
        return $url;
    }
}
