<?php

namespace App\Entities;

use App\Entities\Category;
use App\Entities\Tag;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Post extends Model implements Transformable
{
    use TransformableTrait;

    const PUBLISH = 2;
    const DRAFT   = 1;
    const TRASH   = 0;

    /**
     * [$primary_id description]
     * @var string
     */
    protected $primary_key = 'id';

    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'title', 'slug', 'link', 'excerpt', 'content', 'post_type', 'author', 'feature', 'order', 'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

}
