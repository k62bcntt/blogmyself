<?php

namespace App\Entities;

use App\Entities\Comment;
use App\Entities\Post;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User.
 *
 * @package namespace App\Entities;
 */
class User extends Authenticatable implements Transformable, CanResetPassword
{
    use HasApiTokens, Notifiable;
    use HasRoles;
    use TransformableTrait;
    use CanResetPasswordTrait;

    protected $guard_name = 'api';
    const ACTIVE          = 1;
    const DEACTIVE        = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'display_name', 'avatar', 'email', 'gender', 'phone', 'dob', 'address', 'email_verified', 'created_at', 'status', 'updated_at', 'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function post()
    {
        return $this->hasMany(Post::class);
    }
}
