<?php

namespace App\Entities;

use App\Entities\Post;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Category extends Model implements Transformable
{
    use TransformableTrait;

    protected $primary_key = 'id';

    protected $fillable = ['name', 'slug', 'parent_id', 'description', 'icon' , 'order', 'status'];

    public function posts() {
    	return $this->belongsToMany(Post::class, 'post_id');
    }

}
