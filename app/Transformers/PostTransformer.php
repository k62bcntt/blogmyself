<?php

namespace App\Transformers;

use App\Entities\Post;
use League\Fractal\TransformerAbstract;

/**
 * Class PostTransformer.
 *
 * @package namespace App\Transformers;
 */
class PostTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'user',
    ];

    protected $commentTransformerDefaultIncludes = [];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \Post entity
     * @param \Post $model
     *
     * @return array
     */
    public function transform(Post $model)
    {
        $image = '';
        if ($model->image != '') {
            $image = filter_var($model->image, FILTER_VALIDATE_URL) === false ? asset($model->image) : $model->image;
        }

        return [
            'id'         => (int) $model->id,
            'title'      => $model->title,
            'slug'       => $model->slug,
            'image'      => $image,
            'excerpt'    => $model->excerpt,
            'content'    => $model->content,
            'gallery'    => $model->gallery,
            'author'     => (int) $model->author,
            'post_type'  => $model->post_type,
            'feature'    => $model->feature,
            'order'      => (int) $model->order,
            'status'     => (int) $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }

    public function includeUser(Post $model)
    {
        if (!empty($model->author)) {
            return $this->item($model->author, new UserTransformer);
        }
    }
}
