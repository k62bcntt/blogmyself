<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionTransformer.
 *
 * @package namespace App\Transformers;
 */
class PermissionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];
    /**
     * Transform the \Role entity
     * @param \Role $model
     *
     * @return array
     */

    public function __construct($includes = null)
    {
        if (isset($includes)) {
            $this->setDefaultIncludes($includes);
        }
    }

    public function transform(Permission $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'guard_name' => $model->guard_name,
        ];
    }
}
