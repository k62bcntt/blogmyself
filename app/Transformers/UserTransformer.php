<?php

namespace App\Transformers;

use App\Entities\User;
use App\Transformers\RoleTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'roles',
    ];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \User entity
     * @param \User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        $avatar = '';
        if ($model->avatar != '') {
            $avatar = filter_var($model->avatar, FILTER_VALIDATE_URL) === false ? asset($model->avatar) : $model->avatar;
        }

        return [
            'id'             => (int) $model->id,
            'email'          => $model->email,
            'name'           => $model->name,
            'display_name'   => $model->display_name,
            'avatar'         => $avatar,
            'gender'         => $model->gender,
            'phone'          => $model->phone,
            'dob'            => $model->dob != '0000-00-00' && $model->dob != null ? $model->dob : '',
            'address'        => $model->address,
            'email_verified' => $model->email_verified,
            'status'         => (int) $model->status,
            'created_at'     => $model->created_at,
            'updated_at'     => $model->updated_at,
        ];
    }

    public function includeRoles(User $model)
    {
        return $this->collection($model->roles, new RoleTransformer);
    }
}
