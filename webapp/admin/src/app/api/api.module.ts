import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApiRoutingModule } from './api-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ApiRoutingModule
  ],
  declarations: []
})
export class ApiModule { }
