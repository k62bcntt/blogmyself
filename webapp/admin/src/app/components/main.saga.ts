import { takeEvery, put, select, fork } from "redux-saga/effects";
import * as _ from "lodash";
import * as $ from "jquery";

function toggleSidebarMenu() {
  if ($(window).width() < 991) {
    let LEFT_COL = $(".main_container > #layout > .left_col");
    let LAYOUT = $(".main_container > #layout");
    let isHidden = LEFT_COL.css("display") === "none";

    const SIDE_BAR_WIDTH = 230;
    if (isHidden) {
      LEFT_COL.css({
        display: "block",
        left: -SIDE_BAR_WIDTH,
        width: SIDE_BAR_WIDTH,
        overflow: "hidden"
      });
      LEFT_COL.animate(
        {
          left: 0
        },
        250,
        function() {}
      );
      LAYOUT.animate(
        {
          paddingLeft: SIDE_BAR_WIDTH
        },
        250,
        function() {}
      );
    } else {
      LEFT_COL.animate(
        {
          left: -SIDE_BAR_WIDTH
        },
        250,
        function() {
          LEFT_COL.css({
            display: "none",
            width: 0
          });
        }
      );
      LAYOUT.animate(
        {
          paddingLeft: 0
        },
        250,
        function() {}
      );
    }
  }
}

function* watchToggleSidebarMenu() {
  yield takeEvery("TOGGLE_SIDEBAR_MENU", toggleSidebarMenu);
}

export default _.map([watchToggleSidebarMenu], item => fork(item));
