import { environment } from './../../environments/environment';
import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { AuthService } from "./auth.service";
import * as Cookies from "js-cookie";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) { }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (request.url.indexOf(environment.apiUrl) > -1) {
      request = request.clone({
        setHeaders: {
          "x-styletribute-verification": environment.APP_NAME,
          "x-styletribute-token": environment.APP_TOKEN,
          "Content-Type": "application/json",
        }
      });
    }

    if (request.url.indexOf(environment.testUrl + '/api') > -1) {
      if (request.url.indexOf('upload') > -1) {
        request = request.clone({
          setHeaders: {
            "enctype": "multipart/form-data",
            "Authorization": 'Bearer ' + Cookies.get(environment.JWT_TOKEN_KEY),
            "Accept": "*/*"
          }
        });
      } else {
        request = request.clone({
          setHeaders: {
            "Content-Type": "application/json",
            "Authorization": 'Bearer ' + Cookies.get(environment.JWT_TOKEN_KEY),
          }
        });
      }
    }

    // console.log(request, next);
    return next.handle(request);
  }
}
