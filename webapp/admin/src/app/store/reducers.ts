// import _ from "lodash";
import { combineReducers } from 'redux';

const RootReducer = (state = {}, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default combineReducers({
  RootReducer,
});
