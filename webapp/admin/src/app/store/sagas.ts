import { AppInjector } from './../app-injector';
import { call, put, takeEvery, takeLatest, fork } from 'redux-saga/effects';
import { API_CALL_ERROR } from './action';

import main from '../components/main.saga';

function* watchApiCallError() {
  yield takeEvery(API_CALL_ERROR, function*(action) {
    if ((action as any).error !== undefined) {
      if ((action as any).error.error !== undefined && (action as any).error.error.message !== undefined) {
        // AppInjector.get(NotificationService).show('warning', (action as any).error.error.message, 5000);
      }
    }
  });
}

export default function* sagas() {
  yield [
    ...[fork(watchApiCallError)],
    ...main
  ];
}
